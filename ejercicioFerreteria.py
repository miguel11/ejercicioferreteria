productoPrecio = [[1, 'Brocha', 500], [2, 'Tornillo', 200], [3, 'Martillo', 3000]]
comunas = [[1, 'Quilicura', 3000], [2, 'Santiago', 5000], [3, 'La Granja', 2500], [4, 'La Florida', 6000], [5, 'Renca', 4500]]
x = 0
totalPagar = 0

while x ==  0:
    
    print ("Lista de Productos:")
   
    print ("{:<15} {:<20} {:<10}".format('N° Producto','Producto','Valor'))

    for v in productoPrecio:
        numproducto, nomproducto, valorprod = v
        print ("{:<15} {:<20} {:<10}".format( numproducto, nomproducto, valorprod))




    try:
        producto = int(input("Seleccione un N° de Producto: ")) 
        
        while True:
            try:
                cant = int(input("Ingrese cantidad: "))      
                break          
            except ValueError:
                print("Debes escribir un número para Cantidad.")
                   
    except ValueError:
        print("Debes escribir un número para Producto.")
        continue
    

    productoComprar = productoPrecio[producto-1]
    valorPagar = productoComprar[2] 

    totalPagar = totalPagar + (valorPagar * cant)

    listaComprar= []
    listaComprar.append(productoComprar[1])

    print("Usted a agregado: ", cant, productoComprar[1] + "(S)")


    seguir =  str(input("Desea seguir comprando ? (S/N): "))
    while (seguir != 'N' and seguir != 'S' ):
        print("Debe ingresar S o N")
        seguir =  str(input("Desea seguir comprando ? (S/N): "))
    if (seguir == "N"):
        x = 1
    
print("El valor total de su compra es: ")
print(totalPagar)
valorDespacho = 0
despacho =  str(input("Desea despacho ? (S/N): "))
while (despacho != "N" and despacho != "S"):
     print("Debe ingresar S o N")
     despacho =  str(input("Desea seguir comprando ? (S/N): "))
if (despacho == "S"):
    print ("Comunas disponibles para despacho: ")    
    print ("{:<15} {:<20} {:<10}".format('N° Comuna','Comuna','Valor Despacho'))

    for v in comunas:
        ncomuna, nomcomuna, valordesp = v
        print ("{:<15} {:<20} {:<10}".format( ncomuna, nomcomuna, valordesp))

    comuna = int(input("Ingrese N° Comuna: "))   
    comunaSelec = comunas[comuna - 1]
    print("Usted selecciono la comuna de " + comunaSelec[1] )
    valorDespacho = comunaSelec[2]
    print ("Valor a pagar por el despacho: ")
    print(valorDespacho)


print("********************")
print("Total a Pagar: ")
print(totalPagar + valorDespacho)